﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperFix
{
    public static readonly int UP_DOWN_INDEX = 5;
    public static readonly int LEFT_RIGHT_INDEX = 1;

    public static List<Transform> FindAllTransform(Transform objFind, string _findName)
    {
        List<Transform> _TR = new List<Transform>();

        for (int j = 0; j < objFind.childCount; j++)
        {
            if (objFind.GetChild(j).name.Equals(_findName))
            {
                _TR.Add(objFind.GetChild(j));
            }
        }

        if (objFind.transform.childCount > 0)
        {
            _TR.AddRange(FindChild(objFind.transform, _findName));
        }

        return _TR;
    }

    private static List<Transform> FindChild(Transform findTR, string _findName)
    {
        List<Transform> _childTR = new List<Transform>();

        for (int i = 0; i < findTR.childCount; ++i)
        {
            var findObject = findTR.GetChild(i);

            _childTR.AddRange(FindAllTransform(findObject, _findName));

            if (findObject.childCount > 0)
            {
                _childTR.AddRange(FindChild(findObject, _findName));
            }
        }

        return _childTR;
    }
}
