﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[SerializeField]
public enum eDIRECTION
{
    NONE = -1,
    LEFT = 1,
    RIGHT,
    UP,
    DOWN
}

public class CPad : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    public static event System.Action<int> OnChangeDirection = null;

    private readonly float m_fPadLenght = 0.001f;

    [SerializeField] private Image m_TargetImage = null;
    [SerializeField] private Transform m_trTarget = null;
    private Vector3 orginPos = Vector3.zero;
    
    private void Start()
    {
        orginPos = m_TargetImage.transform.position;
    }

    private bool IsRange(Vector3 _posVector3)
    {
        float lenght = Vector3.Magnitude(m_trTarget.position - _posVector3);

        lenght = Mathf.Abs(Mathf.Abs(100) - Mathf.Abs(lenght));

        if (orginPos == m_TargetImage.rectTransform.position)
        {
            return true;
        }

        if (m_fPadLenght < lenght)
        {
            return false;
        }

        return true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        OnChangeDirection?.Invoke(FindDirection());
     
        //if (false == IsRange(Camera.main.ScreenToWorldPoint(Input.mousePosition)))
        //{
        //    return;
        //}

        Vector2 vecConvert = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        m_TargetImage.rectTransform.position = vecConvert;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        OnChangeDirection?.Invoke((int)eDIRECTION.NONE);

        m_TargetImage.rectTransform.position = orginPos;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        OnChangeDirection?.Invoke((int)eDIRECTION.NONE);

        m_TargetImage.rectTransform.position = orginPos;
    }

    private int FindDirection()
    {
        RectTransform rectTransform = m_TargetImage.rectTransform;
        Vector3 position = rectTransform.position;
        Vector3 position1 = m_trTarget.position;
        float xdf = position1.x - position.x;
        float ydf = position1.y - position.y;

        int nDegree = (int)(RadianToDegree((float)Math.Atan2(ydf, xdf)));

        if (-135 <= nDegree && nDegree <= -45)
        {
            return (int)eDIRECTION.UP;
        }
        else if (45 <= nDegree && nDegree <= 135)
        {
            return (int)eDIRECTION.DOWN;
        }
        else if (-180 <= nDegree && nDegree <= -135 ||
            135f <= nDegree && nDegree <= 180f)
        {
            return (int)eDIRECTION.RIGHT;
        }
        else if (-45 <= nDegree && nDegree < 0 ||
            0 < nDegree && nDegree <= 45)
        {
            return (int)eDIRECTION.LEFT;
        }

        return (int)eDIRECTION.NONE;
    }

    public static float RadianToDegree(float rad)
    {
        return (float)(rad * (180 / Math.PI));
    }
}
