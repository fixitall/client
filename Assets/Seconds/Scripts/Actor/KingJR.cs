﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eKINGJP_STATE
{
    CLIMB = 0,
    YELL,
    MOVE,
    ATTACK,
}

public class KingJR : Actor
{
    public static event System.Action OnChangeInitGame = null;

    [SerializeField]
    private Sprite[] m_ActorAni_RightWork = null;

    [SerializeField]
    private Sprite[] m_ActorAni_LeftWork = null;

    [SerializeField]
    private Sprite[] m_ActorAni_Attack = null;
    [SerializeField]
    private Sprite[] m_ActorAni_Climb = null;
    [SerializeField]
    private Sprite[] m_ActorAni_Idle = null;
    [SerializeField]
    private Sprite[] m_ActorAni_Yell = null;

    private RectTransform m_rtKing = null;
    private int m_nDirection = -1;
    private int m_nState = -1;

    Vector3 startPos = Vector3.zero;
    Vector3 endPos = Vector3.zero;
    private bool isClimb = false;

    int[,] outPutValue = new int[,] { { 0, 4 }, { 4, 9 }, { 9, 5 }, { 5, 10 }, { 10, 14 }, { 14, 19 }, { 19, 15 } ,{ 15, 20 } };

    int nClimbIndex = 0; // 0 hor move 1, virticel Up
    int startindex = 0;
    int lastindex = 0;
    
    protected override void OnCreate()
    {

    }

    protected override void OnLeave()
    {
        
    }

    protected override void OnUpdate()
    {
        if (false == MainGame.Instance.IsAlreadyPlay)
        {
            return;
        }

        switch (m_nState)
        {
            case (int)eKINGJP_STATE.MOVE:
                {
                    Vector2 movePos = new Vector2(m_rtKing.anchoredPosition.x + (m_nDirection * 30.0f) * Time.deltaTime, m_rtKing.anchoredPosition.y);

                    m_rtKing.anchoredPosition = movePos;
                }
                break;
            case (int)eKINGJP_STATE.CLIMB:
                {
                    ProcClimb();
                }
                break;
            case (int)eKINGJP_STATE.YELL:
                {

                }
                break;
            case (int)eKINGJP_STATE.ATTACK:
                {

                }
                break;
        }
    }

    public override void InitActor()
    {
        SetState((int)eKINGJP_STATE.MOVE);

        m_rtKing = this.GetComponent<RectTransform>();

        m_nDirection = 1;

        isClimb = false;

        nClimbIndex = 0;

        startPos = MainGame.Instance.FindWindowPos(0);

        m_rtKing.position = startPos;

        SetState((int)eKINGJP_STATE.CLIMB);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Contains("Window"))
        {
            m_nDirection *= -1;

            MoveANISTATE();
        }

        if (m_nState == (int)eKINGJP_STATE.CLIMB)
        {
            Debug.Log(collision.gameObject.name);

            CDoor cDoor = collision.gameObject.GetComponent<CDoor>();

            if (null != cDoor)
            {
                if (cDoor.GetState() == (int)EDOOR.CLOSE) return;

                int eStateType = Random.Range((int)EDOOR.CLEAR, (int)EDOOR.MAX);

                cDoor.ChangeState(eStateType);
            }
        }
    }

    private void SetState(int _nIndex)
    {
        m_nState = _nIndex;

        switch (_nIndex)
        {
            case (int)eKINGJP_STATE.CLIMB:

                this.GetComponent<Rigidbody2D>().simulated = true;
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;

                ProcAni(m_ActorAni_Climb);
                break;

            case (int)eKINGJP_STATE.ATTACK:
                ProcAni(m_ActorAni_Attack);
                break;

            case (int)eKINGJP_STATE.MOVE:
                MoveANISTATE();
                break;

            case (int)eKINGJP_STATE.YELL:

                MainGame.Instance.DisableWindowsComponentCollider();

                ProcAni(m_ActorAni_Yell);

                this.GetComponent<Rigidbody2D>().simulated = true;
                this.GetComponent<Rigidbody2D>().freezeRotation = false;
                this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

                StartCoroutine(IE_DelayFunc());

                break;
        }
    }

    private void MoveANISTATE()
    {
        if (m_nDirection == -1)
        {
            ProcAni(m_ActorAni_LeftWork);
        }
        else
        {
            ProcAni(m_ActorAni_RightWork);
        }
    }

    private void ProcClimb()
    {
        if (false == isClimb)
        {
            startindex = outPutValue[nClimbIndex, 0];
            lastindex = outPutValue[nClimbIndex, 1];

            startPos = MainGame.Instance.FindWindowPos(startindex);
            endPos = MainGame.Instance.FindWindowPos(lastindex);

            m_rtKing.position = startPos;

            isClimb = true;
        }

        m_rtKing.position = Vector2.LerpUnclamped(m_rtKing.position, endPos, Time.deltaTime * 5);

        if ((Vector3.Distance(m_rtKing.position, endPos) - 90) <= 0.0f && isClimb)
        {
            isClimb = false;

            nClimbIndex++;

            if (nClimbIndex == (outPutValue.Length / 2))
            {
                this.GetComponent<Rigidbody2D>().simulated = false;

                m_rtKing.position = MainGame.Instance.FindWindowPos(MainGame.Instance.MaxCount() - 23);

                SetState((int)eKINGJP_STATE.YELL);
            }
        }
    }

    private IEnumerator IE_DelayFunc()
    {
        yield return new WaitForSeconds(0.5f);

        SetState((int)eKINGJP_STATE.MOVE);

        OnChangeInitGame?.Invoke();
    }
}
