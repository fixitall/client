﻿using System.Collections.Generic;
using UnityEngine;

public class FelixJR : Actor
{
    [SerializeField] private Sprite[] m_Idle = null;
    [SerializeField] private Sprite[] m_Fix = null;

    private RectTransform m_RTPlayer = null;

    private int m_nIndexWindows = 0;
    private int m_nDirection = -1;

    private int m_nBtnCount = 0;
    private CDoor m_cDoor = null;

    protected override void OnCreate()
    {
        this.gameObject.SetActive(false);

        m_RTPlayer = this.GetComponent<RectTransform>();

        KingJR.OnChangeInitGame -= InitActor;
        KingJR.OnChangeInitGame += InitActor;
    }

    protected override void OnLeave()
    {
        CPad.OnChangeDirection -= OnChangeIndex;
        CPad.OnChangeDirection -= OnChangeIndex;

        KingJR.OnChangeInitGame -= InitActor;
        KingJR.OnChangeInitGame += InitActor;
    }

    protected override void OnUpdate()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            OnChangeIndex((int)eDIRECTION.RIGHT);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            OnChangeIndex((int)eDIRECTION.LEFT);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            OnChangeIndex((int)eDIRECTION.UP);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            OnChangeIndex((int)eDIRECTION.DOWN);
        }
    }

    public override void InitActor()
    {
        this.gameObject.SetActive(true);

        InitState();

        CPad.OnChangeDirection -= OnChangeIndex;
        CPad.OnChangeDirection += OnChangeIndex;
    }

    private void OnChangeIndex(int m_index)
    {
        if (m_index == (int)eDIRECTION.NONE)
        {
            return;
        }

        if (m_index == (int)eDIRECTION.RIGHT)
        {
            if (false == MainGame.Instance.PlayerMaxFloor(m_nIndexWindows + 1))
            {
                m_nIndexWindows = m_nIndexWindows + HelperFix.LEFT_RIGHT_INDEX;
            }
        }
        else if (m_index == (int)eDIRECTION.LEFT)
        {
            if (false == MainGame.Instance.PlayerMaxFloor(m_nIndexWindows - 1))
                m_nIndexWindows = m_nIndexWindows - HelperFix.LEFT_RIGHT_INDEX;
        }
        else if (m_index == (int)eDIRECTION.UP)
        {
            if (false == MainGame.Instance.PlayerMaxFloor(m_nIndexWindows + 5))
                m_nIndexWindows = m_nIndexWindows + HelperFix.UP_DOWN_INDEX;
        }
        else if (m_index == (int)eDIRECTION.DOWN)
        {
            if (false == MainGame.Instance.PlayerMaxFloor(m_nIndexWindows - 5))
                m_nIndexWindows = m_nIndexWindows - HelperFix.UP_DOWN_INDEX;
        }

        if (m_nIndexWindows <= 0) { m_nIndexWindows = 0; }

        CDoor curDoor = MainGame.Instance.FindDoor(m_nIndexWindows);

        if (m_cDoor != curDoor)
        {
            m_cDoor = curDoor;

            if (m_cDoor.GetEState() == EDOOR.CLOSE)
                return;

            m_nBtnCount = 0;
        }

        m_nDirection = m_index;

        m_RTPlayer.position = MainGame.Instance.FindWindowPos(m_nIndexWindows);
    }

    public void OnClick_FixWindow()
    {
        if (null == m_cDoor) return;

        if (m_cDoor.GetState() < (int)EDOOR.BROKE_1) return;

        ProcAni(m_Fix);

        if (m_nBtnCount >= 2)
        {
            m_cDoor.InitState((int)EDOOR.CLEAR);

            ProcAni(m_Idle);

            CheckAllFixWindows();

            m_nBtnCount = 0;
        }

        m_nBtnCount++;
    }

    private void CheckAllFixWindows()
    {
        if (MainGame.Instance.IsCheckAllFix())
        {
            string[] paramStrings = new string[3]
            {
                "Congratulation",
                $"<color=cyan>You Fixed it All</color>",
                "one"
            };

            GuiMgr.Instance.ShowMessageBox((_isOK) =>
            {
                GuiMgr.Instance.CloseShowMessageBox();

                MainGame.Instance.InitGame();

                this.gameObject.SetActive(false);

                InitState();

            }, paramStrings);
        }
    }

    private void InitState()
    {
        m_nIndexWindows = 0;
        m_RTPlayer.position = MainGame.Instance.FindWindowPos(m_nIndexWindows);
        m_cDoor = MainGame.Instance.FindDoor(m_nIndexWindows);

        ProcAni(m_Idle);
    }
}
