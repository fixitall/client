﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public abstract class Actor : MonoBehaviour
{
    [SerializeField]
    protected Image m_AniImageTarget = null;

    private IEnumerator IE_Proc = null;

    void Start()
    {
        OnCreate();
    }

    void Update()
    {
        OnUpdate();
    }

    void OnDestroy()
    {
        if (null != IE_Proc)
        {
            StopCoroutine(IE_Proc);

            IE_Proc = null;
        }

        OnLeave();
    }

    protected virtual void ProcAni(Sprite[] sprites)
    {
        if (this.gameObject.activeInHierarchy == false)
        {
            if (null != IE_Proc)
            {
                StopCoroutine(IE_Proc);

                IE_Proc = null;
            }

            return;
        }

        if (null != IE_Proc)
        {
            StopCoroutine(IE_Proc);

            IE_Proc = null;
        }

        Sprite[] targetSprites = sprites;
        
        IE_Proc = IE_ProcAni(targetSprites);

        StartCoroutine(IE_Proc);
    }

    private IEnumerator IE_ProcAni(Sprite[] _targetSprites)
    {
        int startIndex = 0;
        int lastIndex = _targetSprites.Length;

        m_AniImageTarget.sprite = _targetSprites[startIndex];

        if(lastIndex == 1)
            yield break;

        while (true)
        {
            m_AniImageTarget.sprite = _targetSprites[startIndex];

            startIndex++;

            if (lastIndex == startIndex)
            {
                startIndex = 0;
            }

            yield return new WaitForSeconds(0.3f);
        }
    }

    protected abstract void OnCreate();
    protected abstract void OnUpdate();
    protected abstract void OnLeave();

    public abstract void InitActor();
}
