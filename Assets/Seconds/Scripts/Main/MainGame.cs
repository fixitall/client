﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGame : SingletonComponet<MainGame>
{
    [SerializeField] private Transform m_TargetWindows = null;


    private List<CMap> m_cMap = new List<CMap>();

    [SerializeField] private KingJR m_KingJR = null;
    [SerializeField] private FelixJR m_FelixJR = null;

    private List<CDoor> m_trWindows = new List<CDoor>();
    public bool IsAlreadyPlay { get; private set; }

    void Awake()
    {
        InitGame();
    }

    public void InitGame()
    {
        IsAlreadyPlay = false;

        InitMap();
        InitWindows();

        IsAlreadyPlay = true;

        m_KingJR.InitActor();
        
    }

    public bool IsCheckAllFix()
    {
        return m_trWindows.FindAll(x => x.GetState() > (int)EDOOR.CLEAR).Count == 0;
    }

    private void InitMap()
    {
        var findStage = HelperFix.FindAllTransform(m_TargetWindows, "Stage");

        findStage.ForEach(x => m_cMap.Add(x.gameObject.GetComponent<CMap>()));

        m_cMap[0].InitState(0);

        for (int i = 1; i < m_cMap.Count - 1; ++i)
        {
            m_cMap[i].InitState((int)EMAPSTATE.MID);
        }

        m_cMap[m_cMap.Count - 1].InitState((int)EMAPSTATE.TOP);
    }

    private void InitWindows()
    {
        m_trWindows.Clear();

        var findWindows = (HelperFix.FindAllTransform(m_TargetWindows, "door"));

        for (int i = 0; i < findWindows.Count; ++i)
        {
            CDoor door = findWindows[i].GetComponent<CDoor>();

            if (door == null) continue;

            if (i >= 20)
            {
                door.InitState(0);
            }
            else
            {
                door.InitState((int)EDOOR.CLEAR);
            }

            if (isChecker(i))
            {
                door.InitState(0);
            }

            m_trWindows.Add(door);
        }

        EnableWindowsComponentCollider();
    }

    public Vector3 FindWindowPos(int _index)
    {
        return m_trWindows[_index].transform.position;
    }

    public CDoor FindDoor(int _index)
    {
        return m_trWindows[_index].GetComponent<CDoor>();
    }

    public void DisableWindowsComponentCollider()
    {
        m_trWindows.ForEach(x => x.GetComponent<BoxCollider2D>().enabled = false);
    }

    public void EnableWindowsComponentCollider()
    {
        m_trWindows.ForEach(x => x.GetComponent<BoxCollider2D>().enabled = true);
    }

    public bool PlayerMaxFloor(int _floor)
    {
        return _floor > 19;
    }

    public int MaxCount()
    {
        return m_trWindows.Count;
    }

    public bool isChecker(int _nIndex)
    {
        if (_nIndex == 2 || _nIndex == 7)
            return true;

        return false;
    }
}
