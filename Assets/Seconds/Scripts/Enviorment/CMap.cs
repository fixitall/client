﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EMAPSTATE
{
    NONE = -1,
    ONE = 0,
    MID,
    TOP,
}

public class CMap : MonoBehaviour
{
    [SerializeField]
    private Image m_ImageWall = null;

    [SerializeField]
    private Sprite[] m_sprImages = null;

    [SerializeField]
    private GameObject OpenGate = null;

    public void InitState(int _index)
    {
        if (null != m_ImageWall)
        {
            m_ImageWall.sprite = m_sprImages[_index];
        }

        OpenGate?.SetActive((_index == (int)EMAPSTATE.ONE));
    }
}
