﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EDOOR
{
    CLOSE = 0,
    CLEAR,
    BROKE_1,
    BROKE_2,
    MAX
}

public class CDoor : MonoBehaviour
{
    private int m_nState = 0;
    private bool m_bInit = false;

    [SerializeField] private Sprite[] sprites = null;

    public int GetState()
    {
        return m_nState;
    }

    public EDOOR GetEState()
    {
        return (EDOOR)m_nState;
    }

    public void InitState(int _state)
    {
        if (m_bInit) m_bInit = false;

        m_nState = _state;

        this.GetComponent<Image>().sprite = sprites[m_nState];
    }

    public void ChangeState(int _state)
    {
        if (m_bInit) return;

        m_bInit = true;

        m_nState = _state;

        this.GetComponent<Image>().sprite = sprites[m_nState];
    }
}
