﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GuiBase : MonoBehaviour
{
    protected bool								_isStarted = false;
    protected bool								_isFinished = false;

    protected string[]							_params = null;

	protected Dictionary<string, Transform>		_usedTransList = new Dictionary<string, Transform>();

	public void IsRender(bool pIsRender)
	{
		gameObject.SetActive(pIsRender);
	}

    protected void Awake()
    {
        OnCreate();
    }

    protected void Start()
    {
        _isStarted = true;
        _isFinished = false;

        OnEnter();
    }

    protected void OnEnable()
    {
        if (_isStarted)
        {
            _isFinished = false;

            OnEnter();
        }
    }

    protected void OnDisable()
    {
        OnLeave();

        _params = null;
    }

    protected void OnDestroy()
    {
        OnDelete();
    }


    protected void FindTrans()
    {
        RecursiveFind(transform, _usedTransList);
    }

    protected void RecursiveFind(Transform pTrans, Dictionary<string, Transform> pFindTransList)
    {
        if (pFindTransList.ContainsKey(pTrans.name))
        {
            pFindTransList[pTrans.name] = pTrans;
        }

        int childCnt = pTrans.childCount;

        for (int i = 0; i < childCnt; ++i)
        {
            RecursiveFind(pTrans.GetChild(i), pFindTransList);
        }
    }


    public bool IsFinished
    {
        get
        {
            return _isFinished;
        }
    }

    public void Finish()
    {
        _isFinished = true;
    }

    public void SetParameter(string[] pParams)
    {
        _params = pParams;
    }


    public virtual void OnCreate()
    {
    }

    public virtual void OnEnter()
    {
    }

    public virtual void OnFinish()
    {
        Finish();
    }

    public virtual void OnLeave()
    {
    }

    public virtual void OnDelete()
    {
    }

}
