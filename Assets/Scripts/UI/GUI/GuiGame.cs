﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiGame : GuiBase
{
    [SerializeField]
    private GameScript gameController = null;

    [SerializeField] private Text m_PointText = null;
    [SerializeField] private Text m_TimeText = null;
    [SerializeField] private Text m_StageText = null;

    public override void OnCreate()
    {
        base.OnCreate();
    }

    public override void OnEnter()
    {
        base.OnEnter();

        GameScript.OnChangeStage += OnChangeViewStage;
        GameScript.OnChangePoint += OnChangeViewPoint;
        TimeManager.OnChangeTime += OnChangeViewTime;

        gameController.InitializeSettingGameInfo();
    }

    public override void OnFinish()
    {
        base.OnFinish();
    }

    public override void OnLeave()
    {
        base.OnLeave();

        GameScript.OnChangeStage -= OnChangeViewStage;
        GameScript.OnChangePoint -= OnChangeViewPoint;
        TimeManager.OnChangeTime -= OnChangeViewTime;

        gameController.ReleaseLoadGame();
    }

    public override void OnDelete()
    {
        base.OnDelete();
    }

    public void OnChangeViewStage(int _nStage)
    {
        if (null != m_StageText)
            m_StageText.text = $"Stage : {_nStage.ToString("#0")}";
    }

    public void OnChangeViewPoint(int _nPoint)
    {
        if (null != m_PointText)
            m_PointText.text = $"Point\n{_nPoint.ToString()}";
    }

    public void OnChangeViewTime(int _nTime)
    {
        if (null != m_TimeText)
            m_TimeText.text = $"Remain Time : {Mathf.CeilToInt(_nTime).ToString("D2")} sec";
    }
}
