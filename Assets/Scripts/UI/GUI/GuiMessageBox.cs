﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GuiMessageBox : GuiBase
{
    [SerializeField] private GameObject[] m_obTypes = null;
    [SerializeField] private Text m_uiTitle = null;
    [SerializeField] private Text m_uiDesc = null;

    public System.Action<bool> OnButtonClick = null;

    public override void OnCreate()
    {
        base.OnCreate();
    }

    public override void OnEnter()
    {
        base.OnEnter();

        if (null != m_uiTitle)
        {
            m_uiTitle.text = _params[0];
        }

        if (null != m_uiDesc)
        {
            m_uiDesc.text = _params[1];
        }

        if (String.Compare(_params[2], "two", StringComparison.Ordinal) == 0)
        {
            m_obTypes[0].SetActive(true);
            m_obTypes[1].SetActive(false);
        }
        else
        {
            m_obTypes[0].SetActive(false);
            m_obTypes[1].SetActive(true);
        }

        this.GetComponent<RectTransform>().SetAsLastSibling();
    }

    public override void OnFinish()
    {
        base.OnFinish();
    }

    public override void OnLeave()
    {
        base.OnLeave();
    }

    public override void OnDelete()
    {
        base.OnDelete();
    }

    public void OnClick_OK()
    {
        OnButtonClick?.Invoke(true);
    }

    public void OnClick_Cancel()
    {
        GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, false, null);

        OnButtonClick?.Invoke(false);
    }
}
