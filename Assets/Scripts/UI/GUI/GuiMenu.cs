﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiMenu : GuiBase
{
    public override void OnCreate()
    {
        base.OnCreate();
    }

    public override void OnEnter()
    {
        base.OnEnter();
    }

    public override void OnFinish()
    {
        base.OnFinish();
    }

    public override void OnLeave()
    {
        base.OnLeave();
    }

    public override void OnDelete()
    {
        base.OnDelete();
    }

    public void OnClick_MenuStart()
    {
        GameStateMgr.Instance.ChangeState((int)GameStateMgr.ELayerType.MENU, typeof(GSPlay), null);
    }

    public void OnClick_RankList()
    {
        GameStateMgr.Instance.ChangeState((int)GameStateMgr.ELayerType.PLAY, typeof(GSMenu), null);
    }
}
