﻿using System;
using System.Collections.Generic;
#if UNITY_EDITOR
#endif
using UnityEngine;


public class GuiMgr : SingletonComponet<GuiMgr>
{
    public enum ELayerType
    {
        Back,
        Front
    }

    [SerializeField] private Canvas frontCanvas = null;
    [SerializeField] private Canvas backCanvas = null;

    protected int								_limitOfPools = 5;
    protected List<GuiBase>					    _guiEntityPools = new List<GuiBase>();

    protected Dictionary<string, GuiBase>		_showGuiEntityList = new Dictionary<string,GuiBase>();
    protected Dictionary<string, GuiBase>       _hideGuiEntityList = new Dictionary<string, GuiBase>();
    
    public T Find<T>() where T : GuiBase
    {
        GuiBase guiBase;

        if (_showGuiEntityList.TryGetValue(typeof(T).ToString(), out guiBase))
        {
            return guiBase as T;
        }

        return null;
    }

    public T Show<T>(ELayerType pLayer, bool pShow, string[] pParams) where T : GuiBase
    {
        string guiTypeName = typeof(T).ToString();

        if (pShow)
        {
            GuiBase guiBase;

            if (_showGuiEntityList.TryGetValue(guiTypeName, out guiBase))
            {
                return guiBase as T;
            }

            if (_hideGuiEntityList.TryGetValue(guiTypeName, out guiBase))
            {
                guiBase.StopAllCoroutines();
                guiBase.gameObject.SetActive(false);

                _hideGuiEntityList.Remove(guiTypeName);

                _showGuiEntityList.Add(guiTypeName, guiBase);

                guiBase.SetParameter(pParams);

                guiBase.gameObject.SetActive(true);

                return guiBase as T;
            }

            for ( int i = 0; i < _guiEntityPools.Count; ++i )
            {
                guiBase = _guiEntityPools[i];

                if (guiTypeName == guiBase.GetType().ToString())
                {
                    _guiEntityPools.RemoveAt(i);

                    _showGuiEntityList.Add(guiTypeName, guiBase);

                    guiBase.SetParameter(pParams);

                    guiBase.gameObject.SetActive(true);

                    return guiBase as T;
                }
            }

            Transform parentTrans = null;

            switch (pLayer)
            {
                case ELayerType.Back:
                    {
                        parentTrans = backCanvas.transform;
                    }
                    break;

                case ELayerType.Front:
                    {
						parentTrans = frontCanvas.transform;
                    }
                    break;
			}

            GameObject guiGO = (GameObject)GameObject.Instantiate(Resources.Load($"Gui/{guiTypeName}"), parentTrans, true);

            guiGO.name = guiTypeName;
            guiGO.transform.ResetTransform();
            guiGO.GetComponent<RectTransform>().ResetRectTransform();

            guiBase = (GuiBase)guiGO.GetComponent(guiTypeName);

            _showGuiEntityList.Add(guiTypeName, guiBase);

            guiBase.SetParameter(pParams);

            return guiBase as T;
        }
        else
        {
            if (_showGuiEntityList.TryGetValue(guiTypeName, out var guiBase))
            {
                _showGuiEntityList.Remove(guiTypeName);

                _hideGuiEntityList.Add(guiTypeName, guiBase);

                guiBase.OnFinish();
            }

            return guiBase as T;
        }
    }

    protected void Awake()
    {
        
    }

    void Start()
    {
        InvokeRepeating("RefreshPoolQueue", 0.0f, 1.0f);
    }

    protected void Update()
    {
        if (0 == _hideGuiEntityList.Count)
        {
            return;
        }

        List<GuiBase> goToPools = null;

        foreach (GuiBase guiBase in _hideGuiEntityList.Values)
        {
            if (guiBase.IsFinished)
            {
                if (null == goToPools)
                {
                    goToPools = new List<GuiBase>();
                }

                goToPools.Add(guiBase);
            }
        }

        if (null != goToPools)
        {
            foreach (GuiBase guiBase in goToPools)
            {
                guiBase.StopAllCoroutines();
                guiBase.gameObject.SetActive(false);

                _hideGuiEntityList.Remove(guiBase.GetType().ToString());

                _guiEntityPools.Add(guiBase);
            }
        }
    }

    protected void OnDestroy()
    {
        StopAllCoroutines();
    }


    protected void RefreshPoolQueue()
    {
        
            if (0 == _guiEntityPools.Count)
            {
                return;
            }

            int delCount = _guiEntityPools.Count - _limitOfPools;

            if (delCount <= 0)
            {
                return;
            }

            List<GuiBase> removeList = null;

            foreach (GuiBase guiBase in _guiEntityPools)
            {
                if (delCount > 0)
                {
                    delCount--;

                    if (null == removeList)
                    {
                        removeList = new List<GuiBase>();
                    }

                    removeList.Add(guiBase);
                }
                else
                {
                    break;
                }
            }

            if (null != removeList)
            {
                foreach (GuiBase guiBase in removeList)
                {
                    _guiEntityPools.Remove(guiBase);
#if UNITY_EDITOR
                    GameObject.DestroyImmediate(guiBase.gameObject);
#else
                    GameObject.Destroy(guiBase.gameObject);
#endif
                }
            }

    }

    public void ShowMessageBox(Action<bool> _onAction, params string[] _params)
    {
        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, true, _params);

        guiMessageBox.OnButtonClick = _onAction;
    }

    public void CloseShowMessageBox()
    {
        GuiMessageBox guiMessageBox = GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, false, null);
    }
}
