﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum CARD_TYPE
{
    PLAYING = 0,
    END
}

public enum GAMESTATE
{
    MENU,
    PLAYING
}

public static class Helper
{
    private static System.Random rng = new System.Random();

    public static void ResetTransform(this Transform go)
    {
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;
    }

    public static void ResetRectTransform(this RectTransform go)
    {
        go.sizeDelta = Vector2.zero;
        go.localPosition = Vector2.zero;
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}