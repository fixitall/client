﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GSPlay : GameStateBase
{
    public override void OnEnter()
    {
        base.OnEnter();

        GuiMgr.Instance.Show<GuiMenu>(GuiMgr.ELayerType.Front, false, null);
        GuiMgr.Instance.Show<GuiGame>(GuiMgr.ELayerType.Front, true, null);

        TimeManager.OnChangeTimeOver += OnChangeTimeOver;
    }

    public override void OnLeave()
    {
        base.OnLeave();

        TimeManager.OnChangeTimeOver -= OnChangeTimeOver;
    }

    private void OnChangeTimeOver()
    {
        string[] paramStrings = new string[3]
        {
            "미션 실패",
            $"<b>{GameManager.Instance.GetTotalScore().ToString()}</b> \n 을 획득 하셨습니다.",
            "one"
        };

        GuiMgr.Instance.ShowMessageBox((isOK) =>
        {
            GameStateMgr.Instance.ChangeState((int)GameStateMgr.ELayerType.MENU, typeof(GSMenu), null);
        }, paramStrings);
    }
}
