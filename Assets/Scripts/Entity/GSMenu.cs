﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GSMenu : GameStateBase
{
    private AudioClip m_bgmClip = null;

    public override void OnEnter()
    {
        base.OnEnter();

        GuiMgr.Instance.Show<GuiBackGround>(GuiMgr.ELayerType.Back, true, null);
        GuiMgr.Instance.Show<GuiMessageBox>(GuiMgr.ELayerType.Front, false, null);

        GuiMgr.Instance.Show<GuiGame>(GuiMgr.ELayerType.Front, false, null);
        GuiMgr.Instance.Show<GuiMenu>(GuiMgr.ELayerType.Front, true, null);

        GameManager.Instance.ReleaseStateInfo();
        TimeManager.Instance.EndTime();

        if(null == m_bgmClip)
            m_bgmClip = Resources.Load<AudioClip>("Audio/Bgm");

        SoundManager.Instance.PlayBgm(m_bgmClip);
    }

    public override void OnLeave()
    {
        base.OnLeave();
    }
}
