﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;
using System.Security.Cryptography;


public abstract class GameStateBase : MonoBehaviour
{

    protected bool          _isStarted = false;
    protected bool          _isFinished = false;

    protected string[]      _params = null;

    protected void Start()
    {
        _isStarted = true;
        _isFinished = false;

        OnEnter();
    }

    protected void OnEnable()
    {
        if (_isStarted)
        {
            _isFinished = false;

            OnEnter();
        }
    }

    protected void OnDisable()
    {
        OnLeave();

        _params = null;
    }


    public bool IsFinished
    {
        get
        {
            return _isFinished;
        }
    }

    public void Finish()
    {
        _isFinished = true;
    }

    public void SetParameter(params string[] pParams)
    {
        _params = pParams;
    }


    public virtual void OnEnter()
    {
    }

    public virtual void OnLeave()
    {
        Finish();
    }

}
