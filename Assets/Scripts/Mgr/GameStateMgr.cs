﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameStateMgr : SingletonComponet<GameStateMgr>
{
    [SerializeField]
    public enum ELayerType
    {
        MENU = 0,
        PLAY
    }

    protected int                                           _limitOfPools = 0;
    protected List<GameStateBase>                           _gameStateEntityPools = new List<GameStateBase>();

    protected Dictionary<int, GameStateBase>                _activatedGameStateList = new Dictionary<int, GameStateBase>();

    string Stage = "";

    private int m_LayerType = Int32.MinValue;

    public string GetStatusStage() 
    {
        return Stage;
    }

    public void ChangeState(int pLayer, Type pStateType, params string[] pParams)
    {
        m_LayerType = pLayer;

        string stateTypeName = pStateType.ToString();

        GameStateBase gameStateBase;

        if (_activatedGameStateList.TryGetValue(pLayer, out gameStateBase))
        {
            if (null != gameStateBase)
            {
                _activatedGameStateList[pLayer] = null;

                gameStateBase.StopAllCoroutines();
                gameStateBase.gameObject.SetActive(false);

                _gameStateEntityPools.Add(gameStateBase);
            }
        }

        foreach (GameStateBase poolEntity in _gameStateEntityPools)
        {
            if (poolEntity.GetType().ToString() == stateTypeName)
            {
                _gameStateEntityPools.Remove(poolEntity);

                _activatedGameStateList[pLayer] = gameStateBase;

                gameStateBase.SetParameter(pParams);

                gameStateBase.gameObject.SetActive(true);

                return;
            }
        }

        GameObject gameStateGO = new GameObject();

        gameStateGO.name = pStateType.ToString();
        Stage = pStateType.ToString();
        gameStateGO.transform.parent = transform;
        gameStateGO.transform.ResetTransform();

        gameStateBase = (GameStateBase)gameStateGO.AddComponent(Type.GetType(stateTypeName));

        if (false == _activatedGameStateList.ContainsKey(pLayer))
        {
            _activatedGameStateList.Add(pLayer, gameStateBase);
        }
        else
        {
            _activatedGameStateList[pLayer] = gameStateBase;
        }

        gameStateBase.SetParameter(pParams);

        RefreshPoolQueue();
    }


	public bool IsGSPlay()
	{
		if( _activatedGameStateList[0].GetType().ToString() == "GSPlay")
			return true;
		else
			return false;
	}


    protected void Awake()
    {
        InvokeRepeating("RefreshPoolQueue", 0.0f, 1.0f);
    }

    /// <summary>
    ///  업데이트중 종료된 것과 새로 시작 되는 것에대한 오류가 있음.
    /// </summary>
    protected void Update()
    {
        InputProc();

        if (0 == _activatedGameStateList.Count)
        {
            return;
        }

        List<int> goToPools = null;

        foreach (int layer in _activatedGameStateList.Keys)
        {
            GameStateBase gameStateBase = _activatedGameStateList[layer];

            if (null != gameStateBase && gameStateBase.IsFinished)
            {
                if (null == goToPools)
                {
                    goToPools = new List<int>();
                }

                goToPools.Add(layer);
            }
        }

        if (null != goToPools)
        {
            foreach (int layer in goToPools)
            {
                GameStateBase gameStateBase = _activatedGameStateList[layer];

                gameStateBase.StopAllCoroutines();
                gameStateBase.gameObject.SetActive(false);

                _activatedGameStateList.Remove(layer);

                _gameStateEntityPools.Add(gameStateBase);
            }
        }
    }

    protected void OnDestroy()
    {

    }

    private void InputProc()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (m_LayerType == (int)ELayerType.PLAY)
            {
                TimeManager.Instance.PauseTimer();

                string[] paramsString = new string[]
                {
                    "게임 멈춤",
                    "메뉴로 돌아가시겠습니까?",
                    "two"
                };

                GuiMgr.Instance.ShowMessageBox((bool pIsOk) =>
                {
                    if (pIsOk)
                    {
                        ChangeState((int)ELayerType.PLAY, typeof(GSMenu), null);
                    }
                    else
                    {
                        TimeManager.Instance.ResumeTimer();
                    }
                }, paramsString);
            }
            else
            {
                string[] paramsString = new string[]
                {
                    "게임 종료",
                    "게임을 종료하시겠습니까?",
                    "two"
                };

                GuiMgr.Instance.ShowMessageBox((bool pIsOk) =>
                {
                    if (pIsOk)
                    {
#if UNITY_EDITOR
                        EditorApplication.isPlaying = false;
#else
                        Application.Quit();
#endif
                    }
                }, paramsString);
            }
        }

    }

    protected void RefreshPoolQueue()
    {
        if (0 == _gameStateEntityPools.Count)
        {
            return;
        }

        int delCount = _gameStateEntityPools.Count - _limitOfPools;

        if (delCount <= 0)
        {
            return;
        }

        List<GameStateBase> removeList = null;

        foreach (GameStateBase gameStateBase in _gameStateEntityPools)
        {
            if (delCount > 0)
            {
                delCount--;

                if (null == removeList)
                {
                    removeList = new List<GameStateBase>();
                }

                removeList.Add(gameStateBase);
            }
            else
            {
                break;
            }
        }

        if (null != removeList)
        {
            foreach (GameStateBase gameStateBase in removeList)
            {
                _gameStateEntityPools.Remove(gameStateBase);
#if UNITY_EDITOR
                GameObject.DestroyImmediate(gameStateBase.gameObject);
#else
            GameObject.Destroy(gameStateBase.gameObject);
#endif
            }
        }
    }

    public int GetCount_gameStateEntityPools()
    {
        return _gameStateEntityPools.Count;
    }
}
