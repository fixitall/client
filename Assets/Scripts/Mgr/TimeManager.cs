﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : SingletonComponet<TimeManager>
{
    private readonly int TotalTimeOutValue = 60;

    private TimeSpan m_curGameTimer = TimeSpan.Zero;


    private float m_fResultTime = 0.0f; // Todo 게임 시작 후, 종료 하는 시간을 알려줄 것 

    private bool isPause = false;
    private int m_TotalTime = 0;

    public static event System.Action<int> OnChangeTime = null;
    public static event System.Action OnChangeTimeOver = null;

    #region Time Property

    private IEnumerator onProcess = null;
    #endregion

    public void TimeProgress()
    {
        ResumeTimer();

        if (null == onProcess)
        {
            onProcess = (IE_OnProcessTime());
            StartCoroutine(onProcess);
        }
    }

    public void EndTime()
    {
        if (null != onProcess)
        {
            StopCoroutine(onProcess);
            onProcess = null;
        }

        m_TotalTime = 0;
    }

    public void PauseTimer()
    {
        isPause = true;
    }

    public void ResumeTimer()
    {
        isPause = false;
    }

    private IEnumerator IE_OnProcessTime()
    {
        float TimeLeft = 1.0f;
        float nextTime = Time.time + TimeLeft;

        OnChangeTime?.Invoke(TotalTimeOutValue - m_TotalTime);

        while (true)
        {
            if (false == isPause)
            {
                if (Time.time > nextTime)
                {
                    nextTime = Time.time + TimeLeft;

                    m_TotalTime += 1;

                    if (TotalTimeOutValue == m_TotalTime)
                    {
                        m_TotalTime = 0;

                        if (null != OnChangeTimeOver)
                        {
                            OnChangeTimeOver();
                        }

                        EndTime();

                        break;
                    }

                    OnChangeTime?.Invoke(TotalTimeOutValue - m_TotalTime);
                }
            }

            yield return null;
        }
    }

    public TimeSpan GetResultTime()
    {
        return m_curGameTimer;
    }

    public TimeSpan GetCurTimSpan()
    {
        return DateTime.Now.TimeOfDay;
    }
}
