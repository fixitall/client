﻿using UnityEngine;
using System.Collections;

public class SoundManager : SingletonComponet<SoundManager>
{
    public AudioSource efxSource;                  
    public AudioSource musicSource;                
    public float lowPitchRange = .95f;             
    public float highPitchRange = 1.05f;           
    
    public void PlaySingle(AudioClip clip)
    {
        efxSource.clip = clip;

        efxSource.Play();
    }

    public void PlayBgm(AudioClip _clip)
    {
        if(musicSource.clip  == _clip) return;

        musicSource.clip = _clip;

        musicSource.Play();
    }
}