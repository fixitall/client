﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonComponet<GameManager>
{
    private List<int[]> m_stageInfo = new List<int[]>();
    private int[] m_scores = new int[5] { 5, 10, 15, 20, 25 };

    private int nLevel = 1; //레벨
    private int m_nTotalScore = 0;

    void Awake()
    {
        LoadStageInfo();
    }

    private void Start()
    {
        //GameStateMgr.Instance.ChangeState((int)GameStateMgr.ELayerType.MENU, typeof(GSMenu), null);
    }

    private void LoadStageInfo()
    {
        m_stageInfo.Add(new int[2] { 2, 2 });
        m_stageInfo.Add(new int[2] { 2, 3 });
        m_stageInfo.Add(new int[2] { 2, 4 });
        m_stageInfo.Add(new int[2] { 3, 6 });
        m_stageInfo.Add(new int[2] { 4, 6 });
    }

    public int[] GetStageInfo(int _level)
    {
        return m_stageInfo[_level];
    }

    public int GetScore(int _level)
    {
        return m_scores[_level];
    }

    public int GetMaxStage()
    {
        return m_stageInfo.Count;
    }

    public void AddOneLevelup()
    {
        nLevel += 1;
    }

    public int GetLevel()
    {
        return nLevel;
    }

    public void SetTotalScore(int _nTotalScore)
    {
        m_nTotalScore = _nTotalScore;
    }

    public int GetTotalScore()
    {
        return m_nTotalScore;
    }

    public void ReleaseStateInfo()
    {
        nLevel = 1;
        m_nTotalScore = 0;
    }
}
