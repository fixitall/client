﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    public System.Action<Card> OnCallCard = null;

    private int m_nCardType = 0;
    private int m_nNumber = 0;
    private string m_strPath = "";

    private CARD_TYPE m_cardType = CARD_TYPE.PLAYING;

    private IEnumerator onBackProcessEnumerator = null;

    #region Inspector View
    [SerializeField] private Sprite baseSprite = null;
    [SerializeField] private Sprite targetSprite = null;
    [SerializeField] private Image curMyImage = null;
    #endregion

    public void initCard(int _cardType, int _number, string _cardIamgePath)
    {
        m_cardType = CARD_TYPE.PLAYING;
        
        m_nCardType = _cardType;
        m_nNumber = _number;
        m_strPath = _cardIamgePath;

        targetSprite = Resources.Load<Sprite>(m_strPath);
    }

    ~Card()
    {
        m_cardType = CARD_TYPE.PLAYING;
        curMyImage = null;
    }

    public void GoBackApplyImage()
    {
        if (null != onBackProcessEnumerator)
        {
            StopCoroutine(onBackProcessEnumerator);

            onBackProcessEnumerator = null;
        }

        onBackProcessEnumerator = IE_OnDirectorBack();

        StartCoroutine(onBackProcessEnumerator);
    }

    public void LoadApplyImage()
    {
        if (null != curMyImage)
        {
            curMyImage.sprite = targetSprite;
        }
    }

    public string GetImagePath()
    {
        return m_strPath;
    }

    public void OnClick_SendMyNumber()
    {
        OnCallCard?.Invoke(this);
    }

    private IEnumerator IE_OnDirectorBack()
    {
        yield return new WaitForSeconds(0.5f);

        if (null != curMyImage)
        {
            curMyImage.sprite = baseSprite;
        }
    }

    public void SetStateChangeSuccess()
    {
        m_cardType = CARD_TYPE.END;

        StopCardProcess();
    }

    public CARD_TYPE GetCardStateType()
    {
        return m_cardType;
    }

    private void StopCardProcess()
    {
        if (null != onBackProcessEnumerator)
        {
            StopCoroutine(onBackProcessEnumerator);

            onBackProcessEnumerator = null;
        }

        StopAllCoroutines();

        LoadApplyImage();
    }
}
