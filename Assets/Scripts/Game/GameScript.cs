using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Random = System.Random;

public class GameScript : MonoBehaviour
{
    #region CardInfo
    [Serializable]
    public struct CardInfo
    {
        public int nCardType;
        public int nNumber;
        public string strImagePath;

        public CardInfo(int _nCardType, int _nNumber, string _strImagePath)
        {
            nCardType = _nCardType;
            nNumber = _nNumber;
            strImagePath = _strImagePath;
        }
    }
    #endregion

    private readonly int MAX_TYPE_COUNT = 4;
    private readonly int MAX_NUMB_COUNT = 13;

    private int m_nMaxStage = 0;

    #region Mono Inspector Controller
    [SerializeField] private Card baseCard = null;
    [SerializeField] private Transform gridTrns = null;
    [SerializeField] private GridLayoutGroup gridLayoutGroup = null;
    #endregion

    #region Load Card Info And Sprite
    List<CardInfo> m_createInfoCard = new List<CardInfo>();
    List<Card> m_curMyCard = new List<Card>();

    private int nLevel = 0;
    private int[] stageInfoInts = null;
    private int m_addScorePoint = Int32.MinValue;
    private int m_TotalScore = 0;

    private Card savePreviousCard = null;

    private IEnumerator OnProcessLevelup = null;
    private IEnumerator OnProcessTimeUp = null;

    public static event System.Action<int> OnChangeStage = null;
    public static event System.Action<int> OnChangePoint = null;
    #endregion

    private AudioClip m_cardfailClip = null;
    private AudioClip m_cardClip = null;

    private void Awake()
    {
        m_cardfailClip = Resources.Load<AudioClip>("Audio/fail");
        m_cardClip = Resources.Load<AudioClip>("Audio/card");
    }

    void OnDisable()
    {
        ReleaseGame();
        StopAllCoroutines();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            for (int i = 0; i < m_curMyCard.Count; ++i)
            {
                if (m_curMyCard[i].GetCardStateType() == CARD_TYPE.END) continue;

                m_curMyCard[i].LoadApplyImage();
                m_curMyCard[i].GoBackApplyImage();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {

        }
    }

    public void InitializeSettingGameInfo()
    {
        nLevel = GameManager.Instance.GetLevel();

        SettingStage(nLevel);

        InitEventView();

        CreateCard();

        TimeManager.Instance.TimeProgress();
    }

    private void SettingStage(int level)
    {
        int nSettingLevel = level - 1;

        if (nSettingLevel < 0) nLevel = 0;

        m_nMaxStage = GameManager.Instance.GetMaxStage();

        stageInfoInts = GameManager.Instance.GetStageInfo(nSettingLevel);
        m_addScorePoint = GameManager.Instance.GetScore(nSettingLevel);

        int[] colRange = OutPutRandValue(MAX_NUMB_COUNT);

        int rowCount = 1;
        int colCount = Mathf.Abs((int)(stageInfoInts[0] * stageInfoInts[1]) / 2);

        for (int i = 0; i < rowCount; ++i)
        {
            for (int j = 0; j < colCount; ++j)
            {
                int cardType = UnityEngine.Random.Range(1, 4);

                CardInfo copyCardInfo = new CardInfo(cardType, colRange[j], $"Sprite/{cardType.ToString()}_{colRange[j].ToString()}");

                m_createInfoCard.Add(copyCardInfo);
            }
        }

        m_createInfoCard.AddRange(m_createInfoCard);

        m_createInfoCard.Shuffle();
    }

    private int[] OutPutRandValue(int _arrayRange)
    {
        List<int> nRandInts = new List<int>();

        for (int i = 1; i <= _arrayRange; ++i)
        {
            nRandInts.Add(i);
        }

        nRandInts.Shuffle();

        return nRandInts.ToArray();
    }

    private void CreateCard()
    {
        for (int i = 0; i < m_createInfoCard.Count; ++i)
        {
            Card cardinfo = Instantiate(baseCard);
            cardinfo.initCard(m_createInfoCard[i].nCardType, m_createInfoCard[i].nNumber, m_createInfoCard[i].strImagePath);
            
            Transform tr = cardinfo.transform;
            tr.SetParent(gridTrns);
            tr.transform.ResetTransform();

            cardinfo.OnCallCard = card => OnCardController(card);

            m_curMyCard.Add(cardinfo);
        }

        gridLayoutGroup.constraintCount = stageInfoInts[0];
    }

    private void OnCardController(Card _curCard)
    {
        if (_curCard.GetCardStateType() == CARD_TYPE.END)
        {
            if (null != savePreviousCard)
            {
                savePreviousCard.GoBackApplyImage();
                savePreviousCard = null;
            }

            SoundManager.Instance.PlaySingle(m_cardfailClip);

            return;
        }

        AudioClip playClip = null;

        if (null == savePreviousCard)
        {
            _curCard.LoadApplyImage();
            _curCard.GoBackApplyImage();

            savePreviousCard = _curCard;

            playClip = (m_cardfailClip);
        }
        else
        {
            if (false == savePreviousCard.GetImagePath().Equals(_curCard.GetImagePath()))
            {
                #region Go Release

                _curCard.LoadApplyImage();
                _curCard.GoBackApplyImage();

                savePreviousCard.GoBackApplyImage();
                savePreviousCard = null;
                playClip = (m_cardfailClip);

                #endregion
            }
            else
            {
                #region Go Release
                _curCard.LoadApplyImage();
                _curCard.SetStateChangeSuccess();
                savePreviousCard.SetStateChangeSuccess();
                savePreviousCard = null;

                playClip = (m_cardClip);

                ADDScoreFunc();
                #endregion
            }
        }

        SoundManager.Instance.PlaySingle(playClip);
        
        CheckerFinish();
    }

    //���ġ ����
    public void OnClick_Reresh()
    {
        ReleaseLoadGame();

        InitializeSettingGameInfo();
    }

    public void CheckerFinish()
    {
        if (m_curMyCard.Find(x => x.GetCardStateType() == CARD_TYPE.PLAYING))
        {
            return;
        }
        else
        {
            if (OnProcessLevelup != null)
                return;

            OnProcessLevelup = IE_OnDelayLevelUp();

            StartCoroutine(OnProcessLevelup);
        }
    }

    private IEnumerator IE_OnDelayLevelUp()
    {
        TimeManager.Instance.PauseTimer();

        yield return new WaitForSeconds(0.5f);

        ReleaseLoadGame();

        GameManager.Instance.AddOneLevelup();

        if (GameManager.Instance.GetLevel() > m_nMaxStage)
        {
            string[] paramStrings = new string[3]
            {
                "�����մϴ�.",
                $"<b>{m_TotalScore.ToString()}</b> \n �� ȹ�� �ϼ̽��ϴ�.",
                "one"
            };

            GuiMgr.Instance.ShowMessageBox((isOK) =>
            {
                GameStateMgr.Instance.ChangeState((int)GameStateMgr.ELayerType.MENU, typeof(GSMenu), null);
            }, paramStrings);

            yield break;
        }

        InitializeSettingGameInfo();

        OnProcessLevelup = null;
    }

    private void ReleaseGame()
    {
        if (null != OnProcessTimeUp)
        {
            StopCoroutine(OnProcessTimeUp);

            OnProcessTimeUp = null;
        }

        ReleaseLoadGame();
    }

    public void ReleaseLoadGame()
    {
#if UNITY_EDITOR
        for (int i = 0; i < m_curMyCard.Count; ++i)
        {
            DestroyImmediate(m_curMyCard[i].gameObject);
        }
#else
        for (int i = 0; i < m_curMyCard.Count; ++i)
        {
            Destroy(m_curMyCard[i].gameObject);
        }
#endif
        m_createInfoCard.Clear();
        m_curMyCard.Clear();
    }

    public void OnClick_GameBack()
    {
        GameStateMgr.Instance.ChangeState((int)GameStateMgr.ELayerType.MENU, typeof(GSMenu), null);
    }

    private void InitEventView()
    {
        if (null != OnChangeStage)
        {
            OnChangeStage?.Invoke(nLevel);
        }

        if (nLevel == 1)
        {
            m_TotalScore = 0;

            OnChangePoint?.Invoke(m_TotalScore);
        }
    }

    private void ADDScoreFunc()
    {
        m_TotalScore += m_addScorePoint;

        OnChangePoint?.Invoke(m_TotalScore);

        GameManager.Instance.SetTotalScore(m_TotalScore);
    }

}
